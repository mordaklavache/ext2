//! This crate contains integration tests for ext2.

mod boilerplate_structure;
mod testable;

pub use boilerplate_structure::{Attribute, IntTest};
pub use testable::Testable;

/// This main function will be exported.
#[no_mangle]
#[allow(non_snake_case)]
pub fn TESTS(_args: &[&str]) -> Result<Vec<Box<dyn Testable>>, Box<dyn std::error::Error>> {
    // ... OR YOU CAN USE THE `IntTest` STRUCTURE, WHICH IS A VERY HANDY BOILERPLATE.
    let attr = Attribute {
        name: "dummy name test".to_string(),
        terminate: true,
        show_output: Some(true),
        ..Default::default()
    };
    let ok_cargo =
        IntTest::builder("cargo").output(|status: i32, _out: &[u8], _err: &[u8]| status == 0);

    let mut output: Vec<Box<dyn Testable>> = Vec::new();
    output.push(Box::new(
        ok_cargo
            .clone()
            .args(["+stable", "test"])
            .set_attribute(attr.clone().name("dummy stable test")),
    ));
    output.push(Box::new(
        ok_cargo
            .clone()
            .args(["+stable", "build"])
            .set_attribute(attr.clone().name("dummy stable build")),
    ));

    // let features_list = [
    // ];
    // output.extend(features_list.iter().map(|feature| {
    //     Box::new(
    //         ok_cargo
    //             .clone()
    //             .args(["+stable", "test", feature])
    //             .set_attribute(attr.clone().name(format!("stable test: {}", feature))),
    //     ) as Box<dyn Testable>
    // }));
    // output.extend(features_list.iter().map(|feature| {
    //     Box::new(
    //         ok_cargo
    //             .clone()
    //             .args(["+stable", "build", feature])
    //             .set_attribute(attr.clone().name(format!("stable build: {}", feature))),
    //     ) as Box<dyn Testable>
    // }));

    output.push(Box::new(
        ok_cargo
            .clone()
            .args(["+nightly", "test"])
            .set_attribute(attr.clone().name("dummy unstable test")),
    ));
    output.push(Box::new(
        ok_cargo
            .clone()
            .args(["+nightly", "build"])
            .set_attribute(attr.clone().name("dummy unstable build")),
    ));

    let features_list = [
        "--features=unstable",
    ];
    output.extend(features_list.iter().map(|feature| {
        Box::new(
            ok_cargo
                .clone()
                .args(["+nightly", "test", feature])
                .set_attribute(attr.clone().name(format!("unstable test: {}", feature))),
        ) as Box<dyn Testable>
    }));
    output.extend(features_list.iter().map(|feature| {
        Box::new(
            ok_cargo
                .clone()
                .args(["+nightly", "build", feature])
                .set_attribute(attr.clone().name(format!("unstable build: {}", feature))),
        ) as Box<dyn Testable>
    }));
    Ok(output)
}